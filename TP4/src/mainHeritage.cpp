#include <iostream>
#include "Ligne.hpp"
#include "Couleur.hpp"
#include "Point.hpp"
#include "PolygoneRegulier.hpp"
#include "FigureGeometrique.hpp"
#include <vector>
#include "ViewerFigures.hpp"
#include <gtkmm.h>
#include "ZoneDessin.hpp"

int main(int argc, char ** argv){
    //Interface graphique /////
    ViewerFigures notreFenetre(argc,argv);
    notreFenetre._window.set_title("SuperViewer 1.0");
    notreFenetre._window.set_default_size(640,480);
    ZoneDessin dessinFigure;
    notreFenetre._window.add(dessinFigure);
    notreFenetre.run();



    /*//TP4_TEST///////
    Point point0;
    point0._x=0;point0._y=0;

    Point point1;
    point1._x=100;point1._y=200;

    Couleur rouge;
    rouge._r=1;rouge._g=0;rouge._b=0;

    //Ligne ligne(rouge,point0,point1);
    //ligne.afficher();

    Couleur vert;vert._r=0;vert._g=1;vert._b=0;
    Point centrePoly;centrePoly._x=100;centrePoly._y=200;
    PolygoneRegulier pentagone(vert,centrePoly,50,5);
    //pentagone.afficher();
    //std::cout<<pentagone.getNbPoints()<<std::endl;

    std::vector<FigureGeometrique *> figures;
    figures.push_back(new PolygoneRegulier(vert,centrePoly,50,5));
    figures.push_back(new Ligne(rouge,point0,point1));
    for(int i=0;i<figures.size();i++){
        figures.at(i)->afficher();
    }
    for(auto i : figures){
        delete(i);
    }
*/

    return 0;
}
