#include "FigureGeometrique.hpp"
#include "Couleur.hpp"

FigureGeometrique::FigureGeometrique(const Couleur & couleur):
   _couleur(couleur)
{}
const Couleur & FigureGeometrique::getCouleur()const{
    return _couleur;
}
 void FigureGeometrique::afficher(const Cairo::RefPtr<Cairo::Context> & context) const{}
