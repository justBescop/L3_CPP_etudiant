#include <gtkmm.h>
#include "FigureGeometrique.hpp"
#include "ZoneDessin.hpp"
#include "Point.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"

ZoneDessin::ZoneDessin(){
    _figures.push_back(new Ligne({1,0,0},{10,10},{630,10}));
    _figures.push_back(new Ligne({1,0,0},{630,10},{630,470}));
    _figures.push_back(new Ligne({1,0,0},{10,470},{630,470}));
    _figures.push_back(new Ligne({1,0,0},{10,10},{10,470}));
    _figures.push_back(new PolygoneRegulier({0,1,0},{100,200},50,5));
}
ZoneDessin::~ZoneDessin(){
    for(FigureGeometrique* i : _figures){
        delete i;
    }
}
bool ZoneDessin::on_draw(const Cairo::RefPtr<Cairo::Context> & context){
    for(auto i : _figures){
        i->afficher(context);
    }
    return true;
}
