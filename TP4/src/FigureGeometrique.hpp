#ifndef FIGUREGEOMETRIQUE_HPP_
#define FIGUREGEOMETRIQUE_HPP_
#include "Couleur.hpp"
#include <gtkmm.h>
class FigureGeometrique {
    protected: //Protected car seul les classes dérivées auront accès directement à ça
        Couleur _couleur;

    public:
        FigureGeometrique(const Couleur & couleur);
        const Couleur & getCouleur()const;
        virtual void afficher(const Cairo::RefPtr<Cairo::Context> & context)const;
};

#endif
