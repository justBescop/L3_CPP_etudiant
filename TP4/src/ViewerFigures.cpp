#include <gtkmm.h>
#include "ViewerFigures.hpp"

ViewerFigures::ViewerFigures(int argc, char ** argv): _kit(Gtk::Main(argc,argv)), _window(Gtk::Window()){}
void ViewerFigures::run(){
    _window.show_all();
    _kit.run(_window);
}
