#ifndef ZONE_DESSIN_
#define ZONE_DESSIN_
#include <gtkmm.h>
#include <vector>
#include "FigureGeometrique.hpp"
class ZoneDessin : public Gtk::DrawingArea {
    std::vector<FigureGeometrique*> _figures;
    public:
        ZoneDessin();
        ~ZoneDessin();
        bool on_draw(const Cairo::RefPtr<Cairo::Context> & context) override;
        bool gererClic(GdkEventButton* event);

};

#endif
