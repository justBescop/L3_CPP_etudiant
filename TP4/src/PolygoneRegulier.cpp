#include "PolygoneRegulier.hpp"
#include "Point.hpp"
#include <math.h>
#include <iostream>

PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes):
    FigureGeometrique(couleur), _nbPoints(nbCotes), _points(new Point[nbCotes])
{
    Point ajout;
    for(int i = 0 ; i < _nbPoints ; i++){
        float alpha = i*M_PI*2/_nbPoints;
        ajout._x=rayon*cos(alpha)+centre._x;
        ajout._y=rayon*sin(alpha)+centre._y;
        _points[i]=ajout;
    }
}
PolygoneRegulier::~PolygoneRegulier(){
    delete []_points;
}
void PolygoneRegulier::afficher(const Cairo::RefPtr<Cairo::Context> & context)const{
    std::cout<<"PolygoneRegulier "<<_couleur._r<< "_"<<_couleur._g<<"_"<<_couleur._b<<" ";
    for (int i=0;i<_nbPoints;i++){
        std::cout<<_points[i]._x<<"_"<<_points[i]._y<<" ";
    }
    std::cout<<std::endl;
}
int PolygoneRegulier::getNbPoints()const{
    return _nbPoints;
}
const Point & PolygoneRegulier::getPoint(int indice)const{
    return _points[indice];
}
