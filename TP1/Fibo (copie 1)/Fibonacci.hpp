//Fibonacci.hpp

#ifndef FIBONACCI_HPP_
#define FIBONACCI_HPP_

int fibonacciRecursif(int n);
int fibonacciIteratif(int n);

#endif
