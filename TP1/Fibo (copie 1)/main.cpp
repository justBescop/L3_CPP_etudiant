// main.cpp

#include <iostream>
#include "Fibonacci.hpp"

int main (){
	int n =7;
	std::cout<< "FiboRecursif n =" <<n<< " = "<< fibonacciRecursif(n)<<std::endl;
	std::cout<< "FiboIteratif n =" <<n<< " = "<< fibonacciIteratif(n)<<std::endl;
		
	return 0;
}
