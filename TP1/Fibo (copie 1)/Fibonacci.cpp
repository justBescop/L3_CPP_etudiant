#include <iostream>
#include "Fibonacci.hpp"

int fibonacciRecursif(int n){
	if (n==0) { return 0 ;}
	if (n==1 or n==2) { return 1 ;}
	return fibonacciRecursif(n-1)+fibonacciRecursif(n-2);
	
}

int fibonacciIteratif(int n){
	int f0 = 0;
	if(n==0) return 0;
	int f1= 1;
	if(n==1 or n==2) return 1;
	int f2= 1;
	for (int i = 0; i<n-2 ; i++){
	f0 = f1 + f2;
	std::swap(f1 , f0);
    std::swap(f1 , f2);
	}
	return f2;

}
