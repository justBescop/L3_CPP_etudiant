// main.cpp

#include <iostream>
#include "Fibonacci.hpp"
#include "Vecteur3.hpp"

int main (){
    /*int n =7;
	std::cout<< "FiboRecursif n =" <<n<< " = "<< fibonacciRecursif(n)<<std::endl;
	std::cout<< "FiboIteratif n =" <<n<< " = "<< fibonacciIteratif(n)<<std::endl;
    */

    Vecteur3 vecteur1 {2,3,6};
    Vecteur3 vecteur2 {3,4,7};

    //afficher(vecteur);
    vecteur1.afficher();
    std::cout<<vecteur1.norme()<<std::endl;
    std::cout<<produitScalaire(vecteur1,vecteur2)<<std::endl;
    addition(vecteur1,vecteur2).afficher();
	return 0;
}
