//Vecteur3.cpp

#include <iostream>
#include "Vecteur3.hpp"
#include <math.h>

void afficher(Vecteur3 vect){
	std::cout<< "("<<vect.x<<", "<<vect.y<<", "<<vect.z<<")";
}

float produitScalaire(Vecteur3 vect1, Vecteur3 vect2){
    return vect1.x*vect2.x + vect1.y*vect2.y + vect1.z*vect2.z;
}

Vecteur3 addition(Vecteur3 vect1, Vecteur3 vect2){
    return Vecteur3{vect1.x+vect2.x, vect1.y+vect2.y, vect1.z+vect2.z};
}

void Vecteur3::afficher() const{
    std::cout<< "("<<x<<", "<<y<<", "<<z<<")"<<std::endl;
}

float Vecteur3::norme() const{
    return sqrt(pow(x,2)+pow(y,2)+pow(z,2));
}
