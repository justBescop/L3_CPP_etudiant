//Fibo test
#include "Fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonacci) { };

TEST(GroupFibonacci, test_fiboRecursif) {  // test recursif
    int result = fibonacciRecursif(0);
    CHECK_EQUAL(0, result);
    result = fibonacciRecursif(1);
    CHECK_EQUAL(1, result);
    result = fibonacciRecursif(2);
    CHECK_EQUAL(1, result);
    result = fibonacciRecursif(3);
    CHECK_EQUAL(2, result);
    result = fibonacciRecursif(4);
    CHECK_EQUAL(3, result);
}

TEST(GroupFibonacci, test_fiboIteratif) {  // test iteratif
    int result = fibonacciIteratif(0);
    CHECK_EQUAL(0, result);
    result = fibonacciIteratif(1);
    CHECK_EQUAL(1, result);
    result = fibonacciIteratif(2);
    CHECK_EQUAL(1, result);
    result = fibonacciIteratif(3);
    CHECK_EQUAL(2, result);
    result = fibonacciIteratif(4);
    CHECK_EQUAL(3, result);
}
