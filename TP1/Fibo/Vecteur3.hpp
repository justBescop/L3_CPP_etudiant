// Vecteur3.hpp

#ifndef VECTEUR3_HPP_
#define VECTEUR3_HPP_

struct Vecteur3 {
	float x,y,z;

    void afficher() const;
    float norme() const;
};

void afficher(Vecteur3 vect);
float produitScalaire(Vecteur3 vect1, Vecteur3 vect2);
Vecteur3 addition(Vecteur3 vect1, Vecteur3 vect2);

#endif
