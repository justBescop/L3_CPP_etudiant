#include "Livre.hpp"
#include <string>
#include <iostream>

Livre::Livre(){}
Livre::Livre(const std::string & titre, const std::string & auteur, int annee ) : _titre(titre), _auteur(auteur), _annee(annee){

    /////////////////TEST STRING/////////////////

    std::string::size_type findTitre;
    std::string::size_type findAuteur;
    //Trouver retour chariot

    findTitre = titre.find('\n');
    if(findTitre!=std::string::npos){
        throw std::string("erreur : titre non valide ('\n' non autorisé)");
    }
    findAuteur= auteur.find('\n');
    if(findAuteur!=std::string::npos){
        throw std::string("erreur : auteur non valide ('\n' non autorisé)");
    }
    //Trouver point-virgule

    findAuteur= auteur.find(';');
    if(findAuteur!=std::string::npos){
        throw std::string("erreur : auteur non valide (';' non autorisé)");
    }
    findTitre= titre.find(';');
    if(findTitre!=std::string::npos){
        throw std::string("erreur : titre non valide (';' non autorisé)");
    }

}
const std::string & Livre::getTitre()const{
    return(_titre);
}
const std::string & Livre::getAuteur()const{
    return(_auteur);
}
int Livre::getAnnee()const{
    return(_annee);
}
bool Livre::operator<(const Livre &l2) const{
    if(_auteur<l2._auteur)return true;
    else return _auteur == l2._auteur && _titre < l2._titre;
}
bool operator==(const Livre &l1, const Livre &l2){
return l1._auteur == l2._auteur and
        l1._titre == l2._titre and
        l1._annee == l2._annee;
}
std::ostream & operator<<(std::ostream & os , const Livre & l1){
   os << l1.getTitre() << ";" << l1.getAuteur() << ";" << l1.getAnnee();
   return os;
}
