#include "Image.hpp"
#include <string>
#include <fstream>
#include <iostream>
#include <cmath>

Image::Image(int largeur, int hauteur) : _largeur(largeur), _hauteur(hauteur){
    _pixels = new int [_largeur*_hauteur];
}

Image::~Image(){
    delete [] _pixels;
}

Image::Image(const Image & img): _hauteur(img._hauteur), _largeur(img._largeur)
{
    _pixels = new int [_largeur*_hauteur];

    for(int k=0; k<_largeur*_hauteur;k++){
        _pixels[k] = img._pixels[k];
    }
}
const Image & Image::operator=(const Image &img){
    if(&img !=this){
        _hauteur = img._hauteur;
        _largeur = img._largeur;
        _pixels = new int [_largeur*_hauteur];
        for(int k=0;k<_largeur*_hauteur;k++){
            _pixels[k] = img._pixels[k];
        }
    }
    return *this;
}

int Image::getHauteur()const{
    return _hauteur;
}
int Image::getLargeur()const{
   return _largeur;
}
int Image::getPixel(int i, int j)const{
    return _pixels[i*_largeur+j];
}
void Image::setPixel(int i, int j, int couleur){
    _pixels[i*_largeur+j]=couleur;
}
int Image::pixel(int i,int j)const{
    return _pixels[i*_largeur+j];
}
int & Image::pixel(int i, int j){
    return _pixels[i*_largeur+ j];
}
void ecrirePnm(const Image & img, const std::string & nomFichier){
    std::ofstream ofs(nomFichier);
    ofs << "P2" << std::endl;
    ofs << img.getLargeur()<<" " << img.getHauteur() << std::endl;
    ofs << "255" <<std::endl;

    for(int i=0; i<img.getHauteur();i++){
        for (int j=0; j<img.getLargeur();j++){
            ofs << img.pixel(i, j) << " ";
        }
        ofs << std::endl;
    }
}
void remplir (Image & img){
    double l = img.getLargeur();
    for(int j=0; j<img.getLargeur();j++){
        double t =3*2*M_PI*j / l;
        int c=(cos(t) + 1) *127;
        for(int i=0; i<img.getHauteur();i++)
            img.pixel(i,j) = c;
    }
}
Image bordure(const Image & img,int couleur){
    Image img2 = img;
    const int i1 = img2.getLargeur() - 1;
    for(int i=0; i<img2.getHauteur();i++){
        img2.pixel(i,0) = couleur;
        img2.pixel(i, i1) = couleur;
    }
    const int j1= img2.getHauteur() - 1;
    for(int j=0;img2.getLargeur();j++){
        img2.pixel(0,j)=couleur;
        img2.pixel(j1,j)=couleur;
    }
    return img2;
}
